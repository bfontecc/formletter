#include	<stdio.h>
#include	"fl.h"
#include	"ws13.h"

/**
 *	process(fmt, data)
 *
 *	Purpose: read from datafile, format and output selected records
 *	Input:   fmt 		- input stream from format file
 *		 data		- stream from datafile
 *	Output:  copied fmt to stdout with insertions
 *	Errors:  not reported, functions call fatal() and die
 **/
process(FILE *fmt, FILE *data)
{
	symtab_t *tab;

	if ( (tab = new_table()) == NULL )
		fatal("Cannot create storage object","");

	while ( get_record(tab,data) != NO )/* while more data	*/
	{
		mailmerge( tab, fmt );		/* merge with format	*/
		clear_table(tab);		/* discard data		*/
	}
}

/*
 * get_record
 * 
 * retrieves one line of data (ending with newline) from stream
 */

int	get_record(symtab_t *tab, FILE *data)
{
	//
}

void mailmerge( symtab_t *tab, FILE *fmt)
{
	//
}

